.. image:: https://gitlab.com/sumner/sublime-music/-/raw/master/docs/logo/logo.png
   :alt: Sublime Music Logo

Sublime Music is a GTK3 `Revel`_/`Gonic`_/`Subsonic`_/`Airsonic`_/\*sonic client
for the Linux Desktop.

.. _Revel: https://gitlab.com/robozman/revel
.. _Gonic: https://github.com/sentriz/gonic
.. _Subsonic: http://www.subsonic.org/pages/index.jsp
.. _Airsonic: https://airsonic.github.io/

-------------------------------------------------------------------------------

|userdoc|_

.. |userdoc| replace:: **Click HERE for extended user documentation.**
.. _userdoc: https://sumner.gitlab.io/sublime-music/

See the |contributing|_ document for how to contribute to this project.

.. |contributing| replace:: ``CONTRIBUTING.rst``
.. _contributing: https://gitlab.com/sumner/sublime-music/-/blob/master/CONTRIBUTING.rst
